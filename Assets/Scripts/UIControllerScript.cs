﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class UIControllerScript : MonoBehaviour
{
    [SerializeField] private GameObject pausePanel;
    [SerializeField] private GameObject resumeButton;
    [SerializeField] private GameObject levelCleartxt;
    [SerializeField] private Dropdown dropDown;
    [SerializeField] private PlayerController player;
    [SerializeField] private Text instructionText;
    public void SetInstructionText(string value = null)
    {
        instructionText.text = value + "\nPress ESC to pause";
    }

    private Scene currActiveScene;
    void Start() {
        currActiveScene = SceneManager.GetActiveScene();    
    }
    public void changeType()
    {
        player.setTypePlay(dropDown.value);
        if(dropDown.value == 0)
            SetInstructionText("Press Z to connect with tower\n");
        else if(dropDown.value == 1)
            SetInstructionText("Press Mouse Button to connect with tower");

    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            pauseGame();
        }
    }

    public void pauseGame()
    {
        pausePanel.SetActive(true);
        Time.timeScale = 0;
    }

    public void resumeGame()
    {
        Time.timeScale = 1;
        pausePanel.SetActive(false);
    }

    public void restartGame()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(currActiveScene.name);
    }

    public void endGame(string TitleText = null)
    {
        resumeButton.SetActive(false);
        pausePanel.SetActive(true);
        levelCleartxt.SetActive(true);
        dropDown.gameObject.SetActive(false);

        if(string.IsNullOrEmpty(TitleText) == false)
            levelCleartxt.GetComponent<Text>().text = TitleText;
    }
}
