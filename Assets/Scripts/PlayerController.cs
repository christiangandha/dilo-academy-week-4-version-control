﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private UIControllerScript uiControl;

    private Rigidbody2D rb2D;
    public float moveSpeed = 5f;

    public float pullForce = 100f;
    public float rotateSpeed = 360f;
    [SerializeField] private GameObject closestTower;
    [SerializeField] private GameObject hookedTower;
    private bool isPulled = false;

    private AudioSource myAudio;
    private bool isCrashed = false;
    private Vector3 startPosition;

    public enum Type
    {
        ZKey = 0,
        Mouse = 1
        
    }
    public Type typePlay;
    public void setTypePlay(int index)
    {   
        typePlay = (Type) index;
    }

    // Start is called before the first frame update
    void Start()
    {
        rb2D = GetComponent<Rigidbody2D>();
        uiControl = GameObject.Find("Canvas").GetComponent<UIControllerScript>(); //not recommend to use .Find
        myAudio = GetComponent<AudioSource>();
        startPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if(isCrashed == true)
        {
            if(myAudio.isPlaying == false)
            {
                //Restart Scene
                restartPosition();
            }
        }
        else
        {
            rb2D.velocity = -transform.up * moveSpeed;
            // rb2D.angularVelocity = 0f;

            if(Input.GetKey(KeyCode.Z) && typePlay == Type.ZKey && Time.timeScale != 0)
            {
                PlayerHooked();
            }

            if(Input.GetKeyUp(KeyCode.Z) && typePlay == Type.ZKey && Time.timeScale != 0)
            {
                PlayerHookRelease();
            }
            
            if(Input.GetMouseButtonDown(0) && typePlay == Type.Mouse && Time.timeScale != 0)
            {
                Vector2 pos = Input.mousePosition;
                Vector2 mousePos = Camera.main.ScreenToWorldPoint(pos);
                Collider2D hit = Physics2D.OverlapPoint(mousePos);

                if(hit != null)
                {
                    if(hit.gameObject.tag == "Tower")
                    {
                        closestTower = hit.gameObject;
                        PlayerHooked();
                    }
                }
            }
            if(Input.GetMouseButtonUp(0) && typePlay == Type.Mouse && Time.timeScale != 0)
            {
                PlayerHookRelease();
            }
        }   
    }

    public void restartPosition()
    {
        //Set to start position
        transform.position = startPosition;

        //Reset Rotation
        transform.rotation = Quaternion.Euler(0f, 0f, 90f);

        //Set isCrased to false
        isCrashed = false;
        
        if(closestTower)
        {
            closestTower.GetComponent<SpriteRenderer>().color = Color.white;
            closestTower = null;
        }
    }

    private void PlayerHooked()
    {
        if(closestTower != null && hookedTower == null)
            hookedTower = closestTower;
            
        if(hookedTower)
        {
            float distance = Vector2.Distance(transform.position, hookedTower.transform.position);

            //Gravitation toward tower
            Vector3 pullDirection = (hookedTower.transform.position - transform.position).normalized;
            float newPullForce = Mathf.Clamp(pullForce / distance, 30, 100);
            rb2D.AddForce(pullDirection * newPullForce); //barely felt the pullforce
            // print(pullDirection * newPullForce);

            //Angular velocity
            rb2D.angularVelocity = -rotateSpeed / distance;
            isPulled = true;
            // print("Angular Velocity = " + (-rotateSpeed / distance) + " newPullForce = " + newPullForce + " distance = " + distance + " hookedTower " + hookedTower.name);

            if(typePlay == Type.Mouse)
            {
                hookedTower.GetComponent<SpriteRenderer>().color = Color.green;
            }
        }
    }
    
    private void PlayerHookRelease()
    {
        isPulled = false;

        if(typePlay == Type.Mouse)
        {
            if(hookedTower != null)
                hookedTower.GetComponent<SpriteRenderer>().color = Color.white;
        }

        //Reset missing?
        rb2D.angularVelocity = 0f;
        hookedTower = null;
    }

    private void OnCollisionEnter2D(Collision2D other) 
    {
        if(other.gameObject.tag == "Wall")
        {
            //gameObject.SetActive(false);
            
            if(isCrashed == false)
            {
                myAudio.Play();
                rb2D.velocity = Vector3.zero;
                rb2D.angularVelocity = 0f;
                isCrashed = true;
            }

            if(closestTower != null)
            {
                closestTower.GetComponent<SpriteRenderer>().color = Color.white;
                closestTower = null;
            }
            if(hookedTower != null)
            {
                hookedTower.GetComponent<SpriteRenderer>().color = Color.white;
                hookedTower = null;
            }
        }
        
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.tag == "Goal")
        {
            Debug.Log("Level Clear!");
            uiControl.endGame();
        }
    }

    private void OnTriggerStay2D(Collider2D other) {
        if(other.tag == "Tower" && isCrashed == false)
        {
            if(typePlay == Type.ZKey)
            {
                closestTower = other.gameObject;

                //Change tower color back to green as indicator
                other.gameObject.GetComponent<SpriteRenderer>().color = Color.green;
            }
        }
    }

    private void OnTriggerExit2D(Collider2D other) {
        if(isPulled)
            return;
        if(other.tag == "Tower")
        {
            closestTower = null;

            //Change tower color back to normal
            other.gameObject.GetComponent<SpriteRenderer>().color = Color.white;
        }
    }
}
